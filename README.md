# Introduction 

This project aims to discover host adaptation of zoonotic viruses based on genomic signature analysis. We use a kernel-based method for analysis of genomic signatures. 

Dependencies: 
- [Shogun toolbox](http://shogun-toolbox.org) 
- [Biopython](http://biopython.org)

