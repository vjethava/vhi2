"""
TestCase for pst_parser
"""
# test_pst_parser.py --- 
# 
# Filename: test_pst_parser.py
# Description: Testing code for pst_parser
# Author: Vinay Jethava
# Created: Mon Aug  5 19:42:18 2013 (+0200)
# Version: 
# Last-Updated: Tue Aug  6 16:37:51 2013 (+0200)
#           By: Vinay Jethava
#     Update #: 27
# 

# Change Log:
# 
# 
# 

import unittest
from pst import PSNode as PSNode
from pst import PSTree as PSTree 

class Test_pst_parser(unittest.TestCase):
    def setUp(self):
        
        self.leaf = "Node: 4 GA [ 9 0 0 0 ] 9 [ 0 9 0 0 ][ -1 -1 -1 -1  ]"
        self.pst_node =  PSNode.read_pst_node_string(self.leaf)
        
    # def test_parse_pst_node_string(self):
    #     node_id, node_word, pre_count, node_count, post_count, node_children_id = parse_pst_node_string(self.leaf) 
    #     self.assertEqual(node_id, 4) 
    #     self.assertEqual(node_word, 'GA')
    #     self.assertEqual(pre_count, [9, 0, 0, 0])
    #     self.assertEqual(node_count, 9)
    #     self.assertEqual(post_count, [0, 9, 0, 0])
    #     self.assertEqual(node_children_id, []) 

    def test_pst_node(self): 
        result_node = parse_pst_node_string(self.leaf)
        self.assertEqual(result_node.node_word , 'GA') 
        self.assertEqual(result_node.node_id, 4) 
        
        
        
# self.virus_tree='''\
# Name: gi|258551608|gb|CP001722.1| Zymomonas mobilis subsp. mobilis NCIMB 11163, complete genome(start=791246, stop=1.85363e+06)
# Date: 12:35:39, 27 Jun, 2013
# Tree: PST
# Alphabet: DNA
# Number(nodes): 5
# Number(parameters): 15
# Node: 0 # [ 59 10 10 10 ] 89 [ 59 10 10 10 ][ 1 -1 -1 -1  ]
# Node: 1 A [ 30 10 9 10 ] 60 [ 30 10 10 10 ][ 2 -1 4 -1  ]
# Node: 2 AA [ 10 10 0 10 ] 30 [ 10 0 10 10 ][ -1 -1 -1 3  ]
# Node: 3 TAA [ 10 0 0 0 ] 10 [ 10 0 0 0 ][ -1 -1 -1 -1  ]
# Node: 4 GA [ 9 0 0 0 ] 9 [ 0 9 0 0 ][ -1 -1 -1 -1  ]\
# '''


if __name__ == '__main__':
    unittest.main()

    
        
