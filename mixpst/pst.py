# pst.py --- 
# 
# Filename: pst.py
# Description: Implements generic function for parsing Pst Trees.  
# Author: Vinay Jethava
# Created: Mon Aug  5 19:59:41 2013 (+0200)
# Version: 
# Last-Updated: Thu Nov  7 15:22:08 2013 (+0100)
#           By: Vinay Jethava
#     Update #: 43
# 

# Change Log:
# 
# 
# 

class PstNode: 
    def __init__(self): 
        self.node_id = None 
        self.node_word = None 
        self.pre_counts = None 
        self.node_count = None
        self.post_counts = None
        self.child_ids = None
        self.is_leaf = False

    def __str__(self): 
        node_str = "Node: %d %s" % (self.node_id, self.node_word)
        node_str = node_str + " %s %d %s %s" % (self.pre_counts, 
                                                self.node_count,
                                                self.post_counts, 
                                                self.child_ids)
    @classmethod
    def parse_pst_node_string(cls, line, keep_n=False): 
        import re
        result = PstNode()
        counts_expr = re.compile('\[[\-\s0-9]*\]') # matches e.g. [9 0 0 0] 
        id_expr = re.compile('^[0-9]+') # matches number at beginning of line
        symbol_expr = re.compile('[A,C,G,T,N,#]+') # matches word in alphabet
        junk, line  = line.split(": ",1)
        counts_list = [[int(i) for i in re.split(' ', curr_list)[1:5]] 
                       for curr_list in  re.findall(counts_expr, line)] 

        symbol_expr_search = re.search(symbol_expr, line)
        result.node_word = symbol_expr_search.group(0)
        result.pre_counts = counts_list[0] 
        result.post_counts = counts_list[1]
        result.child_ids =  counts_list[2]
        print symbol_expr_search.groups(), line
        print result.node_word, result.pre_counts, result.post_counts, result.child_ids
        return result 
        
    
def parse_pst_node_string(line, keep_n=False): 
    import re
    result = PstNode()
    counts_expr = re.compile('\[[\-\s0-9]*\]') # matches e.g. [9 0 0 0] 
    id_expr = re.compile('^[0-9]+') # matches number at beginning of line
    symbol_expr = re.compile('[A,C,G,T,N,#]+') # matches word in alphabet
    junk, line  = line.split(": ",1)
    counts_list = [[int(i) for i in re.split(' ', curr_list)[1:5]] 
                   for curr_list in  re.findall(counts_expr, line)] 
    result.node_word = re.search(symbol_expr, line).group(0)
    result.pre_counts = counts_list[0] 
    result.post_counts = counts_list[1]
    result.child_ids =  counts_list[2]
    print result.pre_counts, result.post_counts, result.child_ids
    return result 

# def read_node_str(self, node_str): 
#     """
#     Parses node string for pst tree
#     """
#     assert(node_str.startswith('Node: ')==True, 
#            'Not a pst node string: %s' % node_str)
#     # discard "Node: " from node_str
#     node_str = node_str[6:]
#     str_list = node_str.split(" ") 
     
        
