# test_binary.py --- 
# 
# Filename: test_binary.py
# Description: Test binary classificication 

# Author: Vinay Jethava
# Created: Wed Nov  6 17:03:04 2013 (+0100)
# Version: 
# Last-Updated: Thu Nov  7 11:52:53 2013 (+0100)
#           By: Vinay Jethava
#     Update #: 11
# 

# Change Log:
# 
# 
# 

from modshogun import * 
from Bio import Seq, SeqIO 

TEST_FILE   = '/home/jethava/vhi2/data/binary47.txt'
PROFILE_DIR = '/home/jethava/vhi2/data/profiles/'

def read_file(input_file, profile_dir): 
    ''' 
    Reads input using accession ids, 
    ''' 
    result = [] 
    with open(input_file, 'r') as fid: 
        lines = fid.readlines() 
        for line in lines: 
            line = line.strip() 
            accession_0, accession_1, label = line.split(' ')
            fasta_file_0 = profile_dir + '/' + accession_0 + '.fasta' 
            fasta_file_1 = profile_dir + '/' + accession_1 + '.fasta' 
            seq_0 = SeqIO.parse(open(fasta_file_0, "rU"), "fasta").next()
            seq_1 = SeqIO.parse(open(fasta_file_1, "rU"), "fasta").next()
            result.append((seq_0, seq_1, label)) 
    return result 

