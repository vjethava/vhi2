# This scripts basic FASTA reading and kernel comparison 

#!/usr/bin/env python 

from Bio import SeqIO 
import numpy
from modshogun import * 

############################################################
## Example Fasta file
############################################################
fasta_file='../data/profiles/Bos_taurus.UMD3.1.73.cdna.all.fa' 

############################################################
# first dataset
############################################################
traindna = ['CGCACGTACGTAGCTCGAT',
            'CGACGTAGTCGTAGTCGTA',
            'CGACGGGGGGGGGGTCGTA',
            'CGACCTAGTCGTAGTCGTA',
            'CGACCACAGTTATATAGTA',
            'CGACGTAGTCGTAGTCGTA',
            'CGACGTAGTTTTTTTCGTA',
            'CGACGTAGTCGTAGCCCCA',
            'CAAAAAAAAAAAAAAAATA',
            'CGACGGGGGGGGGGGCGTA']
label_traindna = numpy.array(5*[-1.0] + 5*[1.0])
testdna = ['AGCACGTACGTAGCTCGAT',
           'AGACGTAGTCGTAGTCGTA',
           'CAACGGGGGGGGGGTCGTA',
           'CGACCTAGTCGTAGTCGTA',
           'CAAAAAAAAAAAACCAATA',
           'CGACGGCCGGGGGGGCGTA']
label_testdna = numpy.array(5*[-1.0] + 5*[1.0])

############################################################
# second dataset - different length strings
############################################################
traindna2 = ['AGACAGTCAGTCGAT',
             'AGCAGTCGTAGT',
             'AGCAGGGGGGGGG',
             'AGCAATCGTAGT',
             'AGCAACACG',
             'ACCCCCCCCC',
             'AGCAGGGGGGGGGGGAGTC']
label_traindna2 = numpy.array(5*[-1.0] + 5*[1.0])
testdna2 = ['CGACAGTCAGTCGATAGCT',
            'CGCAGTCGTAGTCGTAGTC',
            'ACCAGGGGGGGGGGTAGTC',
            'AGCAATCGTAGTCGTAGTC',
            'AGCCACACGTTCTCTCGTC',
            'AGCAATCGTAGTCGTAGTC',
            'AGCAGTGGGGTTTTTAGTC',
            'AGCAGTCGTAAACGAAAAC',
            'ACCCCCCCCCCCCAACCTC',
            'AGCAGGAAGGGGGGGAGTC']
label_testdna2 = numpy.array(5*[-1.0] + 5*[1.0])

def read_fasta():  
    ''' This reads the Fasta file ''' 
    handle = open(fasta_file, 'r') 
    first_record = SeqIO.parse(open(fasta_file, "rU"), "fasta").next()
    for record in SeqIO.parse(handle, 'fasta'): 
         print record.description
         print record.seq
    handle.close()

def test_WeightedDegreeStringKernel(): 
    ''' Test to compare two strings of equal size using Shogun kernels'''
    from modshogun import StringCharFeatures, DNA 
    from modshogun import WeightedDegreeStringKernel
    ############################################################ 
    # Generate string features
    ############################################################
    feats_train = StringCharFeatures(traindna, DNA)
    feats_test = StringCharFeatures(testdna, DNA) 
    print feats_train
    ############################################################
    # Test Weighted Kernel - strings of same size required
    ############################################################
    degree = 3 
    weighted_kernel = WeightedDegreeStringKernel(feats_train, feats_test, degree)
    kernel_matrix =  weighted_kernel.get_kernel_matrix()    
    print kernel_matrix.shape

def test_order_statistics():
    ''' 
    Test of kernel on string of different sizes using feature statistics

    @note Uses StringUlongFeatures() to convert to ulong
    @note order - order of the feature extractor
    
    '''
    order = 3
    reverse  = False
    gap = 0
    charfeat = StringCharFeatures(DNA) 
    charfeat.set_features(traindna2)
    feats_train=StringUlongFeatures(charfeat.get_alphabet())
    feats_train.obtain_from_char(charfeat, order-1, order, gap, reverse)

    # needs preprocessing. 
    preproc=SortUlongString()
    preproc.init(feats_train)
    feats_train.add_preprocessor(preproc)
    feats_train.apply_preprocessor()

    charfeat=StringCharFeatures(DNA)
    charfeat.set_features(traindna)
    feats_test=StringUlongFeatures(charfeat.get_alphabet())
    feats_test.obtain_from_char(charfeat, order-1, order, gap, reverse)
    feats_test.add_preprocessor(preproc)
    feats_test.apply_preprocessor()

    print "traindna2 features:\n", feats_train
    print "traindna features:\n", feats_test

    use_sign=False
    common_ulong_string_kernel=CommUlongStringKernel(feats_train, feats_train, use_sign) 
    kernel_matrix = common_ulong_string_kernel.get_kernel_matrix() 
    print "kernel size is :", kernel_matrix.shape, "\n", kernel_matrix 
    return common_ulong_string_kernel
    
def test_CommWordStringKernel(): 
    ############################################################
    ## Test CommWordStringKernel
    ############################################################
    from modshogun import CommUlongStringKernel
    from modshogum import StringUlongFeatures, DNA
    
 
def compare_two_sequences(): 
    ''' This compares two sequences using standard Shogun kernels ''' 
    #
    # @TODO Kernel comparison not working.
    #
    handle = open(fasta_file, 'r') 
    record_iterator = SeqIO.parse(open(fasta_file, "rU"), "fasta") 
    first_record = record_iterator.next()
    second_record = record_iterator.next()
        
if __name__=='__main__':
    test_order_statistics() 
    # test_WeightedDegreeStringKernel() 
    
