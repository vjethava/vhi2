# process_ndna.py --- 
# 
#!/usr/bin/env python 
#  
# Filename: process_ndna.py
# Description: This function handles generic DNA characters (in particular N (any)) 
# 
# Currently implemented - remove all N sequences before using kernel 
#  
# Author: Vinay Jethava
# Created: Thu Nov 03 17:13:58 2013 (+0100)
# Version: 
# Last-Updated: Thu Nov 14 17:49:53 2013 (+0100)
#           By: Vinay Jethava
#     Update #: 29
# 

# Change Log:
# 
# 
# 
import sys 
import tempfile
from Bio import Seq, SeqIO 
from common import keyboard 

def remove_ndna(input_fasta_file, output_fasta_file=None): 
    ''' 
    Removes N-DNA to obtain reduced sequence 

    Writes a new output file. If no output file is provided 
    ''' 
    
    try:
        if output_fasta_file is None: 
            ofid = tempfile.NamedTemporaryFile('w', delete=False)
        else: 
            ofid = open(output_fasta_file, 'w') 
    except IOError: 
        print >> sys.stderr, 'Cannot open file for writing: ', output_fasta_file
    output_records = []             
    with open(input_fasta_file, 'rU') as fid: 
        for record in SeqIO.parse(fid, 'fasta'): 
            print record.description
            print "SEQ length: ", len(record.seq)             
            record.seq = record.seq.ungap('N')             
            print "SEQ reduced length: ", len(record.seq) 
            if len(record.seq) > 0: 
                output_records.append(record)

    if len(output_records) > 0: 
        print 'Writing %d modified records to output file: %s\n' % (len(output_records), ofid.name) 
        SeqIO.write(output_records, ofid, 'fasta')
    ofid.close() 

if __name__=='__main__': 
    input_fasta_file = sys.argv[1]
    print "INPUT FILE: ", input_fasta_file
    remove_ndna(input_fasta_file) 
    
