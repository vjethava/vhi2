# vhi_options.py --- 
# 
# Filename: vhi_options.py
# Description: This file contains all default options 
# Author: Vinay Jethava
# Created: Fri Aug  2 11:55:19 2013 (+0200)
# Version: 
# Last-Updated: Wed Oct 30 16:44:18 2013 (+0100)
#           By: Vinay Jethava
#     Update #: 24
# 

# Change Log:
# 
# 
# 
import logging 

## I/O paths 
FILE_SUFFIX = '.tree'
PROFILE_DIR = 'profiles/' 

## Program options
""" Symbols in the alphabet """ 
VOCABULARY = ['A', 'C', 'G', 'T', 'N']


""" Keep symbol N """
KEEP_N = True 

""" Only use leaves to compute the statistics """
LEAVES_ONLY = True

""" Whether to use pseudocounts in case of zero node count"""
USE_PSEUDO_COUNTS = False


## logging
LOGGER_LEVEL = logging.INFO
LOGGER_FORMAT = '%(asctime)s %(funcName)-10s:%(lineno)-4d - %(message)s'
LOG_FILE = "vhi_classify.log"


def get_logger(log_file=LOG_FILE, 
            logger_level=LOGGER_LEVEL, 
            logger_format=LOGGER_FORMAT
            ): 
    """ Provides standard logging

    Keyword arguments: 
    logger_level  -- Logging level (default: logging.DEBUG)
    logger_format -- Logging format (see @LOGGER_FORMAT)
    
    """
    logging.basicConfig(filename=log_file, level=logger_level)
    logger = logging.getLogger()
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logger_level)
    # add formatter to ch
    formatter = logging.Formatter(logger_format)
    ch.setFormatter(formatter)
    # add ch to logger
    logger.addHandler(ch)
    logger.setLevel(logger_level)
    return logger

# global logger 
vhi_logger = get_logger()


