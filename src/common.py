#
# Copyright (c) 2010 Vinay Jethava
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
"""
This file has functions providing general functionality
"""
# import code
import sys
from pydoc import help




def keyboard(banner=None):
    ''' Function that mimics the matlab keyboard command '''
    # use exception trick to pick up the current frame
    try:
        raise None
    except:
        frame = sys.exc_info()[2].tb_frame.f_back
    print "\n # Use 0/0 to exit :) Happy debugging!"
    # evaluate commands in current namespace
    namespace = frame.f_globals.copy()
    namespace.update(frame.f_locals)
    try:
        from IPython.Shell import IPShellEmbed
        ipshell = IPShellEmbed()
        ipshell(banner, namespace) 
    except ImportError: 
        import code
        code.interact(banner=banner, local=namespace)
    except SystemExit:
        return

def cleanlineEndings(file, ending='\n'):
    import os
    text =  open(file, 'r').read()
    fileBak = file + '.bak'
    open(fileBak, 'w').write(text)
    possibleEndings = ['\r\n', '\n', '\r']
    for oldE in possibleEndings:
        if (not oldE == ending) and (oldE in text):
            text = text.replace(oldE, ending)
    open(file, 'w').write(text)


    
