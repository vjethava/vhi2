# get_fasta.py ---  
#
# Filename: get_fasta.py
# Description: Retrieves FASTA files for plasmid list 
# Author: Vinay Jethava
# Created: Wed Oct 30 12:56:36 2013 (+0100)
# Version: 
# Last-Updated: Thu Nov 14 17:58:18 2013 (+0100)
#           By: Vinay Jethava
#     Update #: 78
# 

# Change Log:
# 
# 
# 

from Bio import SeqIO
from Bio.Seq import Seq 
from Bio.Alphabet import generic_dna 
from Bio import Entrez
from vhi_options import vhi_logger as logger 
import sys 



virus_accession_file = '/home/jethava/vhi2/data/virus_accessions.txt'

hosts_file = '/home/jethava/vhi2/data/host_ids.txt'
profile_dir = '/home/jethava/vhi2/data/profiles/' 

RETMAX = 10 # maximum number of queries to look at 

def get_fasta_by_id(entrez_id, out_file=None, entrez_db='nuccore'): 
    Entrez.email = 'vjethava@gmail.com' 
    handle = Entrez.efetch(db=entrez_db, id=entrez_id, rettype="gb")
    result = SeqIO.read(handle, "genbank")
    if out_file is None: 
        out_file  = str(entrez_id) + '.fasta' 
    output_handle = open(out_file, 'w')
    SeqIO.write(result, output_handle, 'fasta')
    output_handle.close() 
    logger.info('Written FASTA file: %s ' % (out_file))  
    
def get_fasta(search_term, out_file, retmax = RETMAX): 
    ''' 
    Returns first hit from nuccore database and saves FASTA file

    @argument search_term
    @argument out_file:   Output Fasta file. 

    Allows choosing best Fasta file 
    
    ''' 
    logger.info("Searching Entrez for term: %s" % (search_term)) 
    handle = Entrez.esearch(db="nuccore", term=search_term, usehistory="y")
    search_results = Entrez.read(handle) 

    gi_list = search_results["IdList"] 
    webenv = search_results["WebEnv"] 
    query_key = search_results["QueryKey"]

    if len(gi_list) == 1:
        accession_id = gi_list[0]
        logger.info("Found single record: %s" % (accession_id)) 
        get_fasta_by_id(accession_id, out_file) 
    else: 
        logger.info("Found %d records, considering first %d" % (len(gi_list), retmax)) 
        gi_list = gi_list[:retmax]          
        accession_id = ",".join(gi_list) 
        handle2 = Entrez.efetch(db="nuccore" , id=accession_id, rettype="xml")   
        print handle2 
        records = Entrez.read(handle2)
        print records
        logger.info("Records fetched: %d" % (len(records))) 
        counter = 1 
        for record2 in records: 
            print counter, record2 
            counter = counter + 1 
        choice = raw_input("Choose best record: ")
        counter = 1 
        best_record = None
        for record2 in records: 
            if counter == choice: 
                best_record = record2
        counter = counter+1 
        record2 = best_record 
    
        

def get_fasta_by_accession_list(file_name,  profile_dir): 
    '''
    Retrieve FASTA for accession ids given in file

    @argument file_name: File containing list of search terms
    @argument profile_dir: Output directory for saving the resulting fasta files
    
    '''
    with open(file_name, 'r') as fid: 
        lines = map(lambda x: x.strip() , fid.readlines() )
        for search_term in lines: 
            out_file_name =  search_term.replace(' ', '_')
            out_file = profile_dir + out_file_name + ".fasta"
            get_fasta(search_term, out_file)
        
if __name__ == '__main__':
    gb_id = sys.argv[1]
    get_fasta_by_id(gb_id) 
